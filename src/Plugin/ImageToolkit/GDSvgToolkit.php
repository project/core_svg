<?php

namespace Drupal\core_svg\Plugin\ImageToolkit;

use Drupal\system\Plugin\ImageToolkit\GDToolkit;

/**
 * Class GDSvgToolkit.
 *
 * @package Drupal\core_svg\Plugin\ImageToolkit
 *
 * @ImageToolkit(
 *   id = "gd:svg",
 *   title = @Translation("GD2 image manipulation toolkit with svg support")
 * )
 */
class GDSvgToolkit extends GDToolkit {

  const PLUGIN_ID = 'gd:svg';

  /**
   * Needed variables.
   */
  const NEEDED_VARIABLES = ['width', 'height'];

  /**
   * Extensions.
   */
  const EXTENSIONS = ['svg'];

  /**
   * A svg image data.
   *
   * @var array|null
   */
  protected $svg = NULL;

  /**
   * Retrieves the svg image data.
   *
   * @return array|null
   *   The svg image data, or NULL if not svg.
   */
  public function getSvg() {
    if (empty($this->svg)) {
      $this->loadSvg();
    }
    return $this->svg;
  }

  /**
   * Loads a svg image data if the file is svg.
   *
   *@return bool
   *  TRUE or FALSE.
   */
  protected function loadSvg() {
    // Get realpath of public file.
    $source = $this->getSource();
    if ($wrapper = $this->streamWrapperManager->getViaUri($source)) {
      $source = $wrapper->realpath();
    }

    // Mime type.
    if (file_exists($source)) {
      $mime_type = mime_content_type($source);
    }

    // Load svg data, array or null.
    $this->svg = NULL;

    if (isset($mime_type) && strpos($mime_type, 'image/svg') !== FALSE) {
      $file_contents = file_get_contents($source);
      if ($file_contents && ($data = simplexml_load_string($file_contents))) {
        $this->svg = [];
        foreach ($data->attributes() as $attribute => $value) {
          if (!in_array($attribute, static::NEEDED_VARIABLES)) {
            continue;
          }
          $this->svg[$attribute] = (string) $value;
        }
        $this->svg['mime_type'] = $mime_type;
        $this->svg['simplexml'] = $data;
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isValid() {
    return parent::isValid() ?: (bool) $this->getSvg();
  }

  /**
   * {@inheritdoc}
   */
  public function getWidth() {
    $width = parent::getWidth();

    if ($svg_data = $this->getSvg()) {
      $width = $svg_data['width'] ?? $width;
    }

    return $width;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeight() {
    $height = parent::getHeight();

    if ($svg_data = $this->getSvg()) {
      $height = $svg_data['height'] ?? $height;
    }

    return $height;
  }

  /**
   * {@inheritdoc}
   */
  public function getMimeType() {
    $mime_type = parent::getMimeType();

    if (!$mime_type && ($svg_data = $this->getSvg())) {
      $mime_type = $svg_data['mime_type'] ?? $mime_type;
    }

    return $mime_type;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSupportedExtensions() {
    return array_merge(parent::getSupportedExtensions(), static::EXTENSIONS);
  }

}
